import Head from "next/head";
import Image from "next/image";
import styles from "../styles/Confirmation.module.scss";
import { Box, Container, Button, TextInput, Footer } from "../components";
import { useRouter } from "next/router";

export default function Confirmation() {
  const router = useRouter();

  return (
    <>
      <Head>
        <title>Vendrell Method: Fitness Program</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Container>
        <Box type="down">
          <div className={styles.body}>
            <Image
              className={styles.image}
              src="/logo.png"
              height={150}
              width={250}
              alt="Area Sports Logo"
            />
            <h2>Gracias por unirte!</h2>
            <Image
              className={styles.image}
              src="/checkmark.png"
              height={100}
              width={100}
              alt="Area Sports Logo"
            />
            <Button onClick={() => router.push('/')}>Iniciar sesión</Button>
            <Footer />
          </div>
        </Box>
      </Container>
    </>
  );
}
