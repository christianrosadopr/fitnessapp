import NextAuth from "next-auth";
import CredentialsProvider from "next-auth/providers/credentials";
import { useRouter } from "next/router";

export default NextAuth({
  providers: [
    CredentialsProvider({
      // The name to display on the sign in form (e.g. "Sign in with...")
      name: "Credentials",
      // The credentials is used to generate a suitable form on the sign in page.
      // You can specify whatever fields you are expecting to be submitted.
      // e.g. domain, username, password, 2FA token, etc.
      // You can pass any HTML attribute to the <input> tag through the object.
      credentials: {
        username: {
          label: "Username",
          type: "text",
          placeholder: "juandelcampo",
        },
        password: { label: "Password", type: "password" },
      },
      async authorize(credentials, req) {
        const { username, password } = credentials;
        // Add logic here to look up the user from the credentials supplied
        const res = await fetch(
          `${process.env.API}/users/auth/login`,
          {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({
              username: username,
              password: password,
            }),
          }
        );

        const user = await res.json();
        // if (!res.ok) {
        //   throw new Error(user.exception);
        // }
        // If no error and we have user data, return it
        if (user) {
          // Any object returned will be saved in `user` property of the JWT
          return user;
        } else {
          // If you return null then an error will be displayed advising the user to check their details.
          return null;

          // You can also Reject this callback with an Error thus the user will be sent to the error page with the error message as a query parameter
        }

        // if (credentials.username === user.username) {
        //   return user;
        // }
      },
    }),
  ],
  secret: process.env.JWT_SECRET,
  // pages: "/login",
  callbacks: {
    // async jwt({ token, user, account }) {
    //   if (account && user) {
    //     return {
    //       ...token,
    //       accessToken: user.data.token,
    //       refreshToken: user.data.refreshToken
    //     };
    //   }
    // },

    // async session({ session, token }) {
    //   session.user.accessToken = token.accessToken;
    //   session.user.refreshToken = token.refreshToken;
    //   session.user.accessTokenExpires;
    // },
    // async signIn({ user }) {
    //   // const router = useRouter();
    //   if (user.username === "danielvendrell") {
    //     return "/admin"
    //   } else if (user.trainingProgram > 0) {
    //     return `/program/${user.trainingProgram}`;
    //   }
    // },
  },

  theme: {
    colorScheme: "light",
    brandColor: "#e37438",
    logo: "/logo.png",
  },

  debug: process.env.NODE_ENV === "development",
});
