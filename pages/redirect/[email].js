import Head from "next/head";
import Image from "next/image";
import styles from "../../styles/Confirmation.module.scss";
import { Box, Container, Button, TextInput, Footer } from "../../components";
import { useRouter } from "next/router";

export async function getServerSideProps(context) {
  const { email } = context.params;
  const res = await fetch(
    `${process.env.API}/users/findByEmail/${email}`
  );
  const user = await res.json();

  if (!user) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      user,
    }, // will be passed to the page component as props
  };
}

export default function Redirect({ user }) {
  const router = useRouter();
  

  return (
    <>
      <Head>
        <title>Vendrell Method: Fitness Program</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Container>
        <Box type="down">
          <div className={styles.body}>
            <h2>Cuenta verificada</h2>
            <Button
              onClick={() => {
                if (user.username === "danielvendrell") {
                  router.push("/admin");
                } else {
                  router.push(`/program/${user.trainingProgram}`);
                }
              }}
            >
              Continuar
            </Button>

            <Footer />
          </div>
        </Box>
      </Container>
    </>
  );
}
