import Head from "next/head";
import Image from "next/image";
import { useState } from "react";
import styles from "../styles/Register.module.scss";
import { useRouter } from "next/router";
import { Box, Container, Button, TextInput, Footer } from "../components";

export default function Register() {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");

  const router = useRouter();

  async function submitForm() {
    await fetch(`https://258dv5hvol.execute-api.us-east-1.amazonaws.com/production/users`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        username: username,
        password: password,
        firstName: firstName,
        lastName: lastName,
        email: email,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        router.push("/confirmation");
      })
      .catch((err) => {
        alert("Error al someter información!");
        console.log(err);
      })
      .finally(() => router.push("/"));
  }
  return (
    <>
      <Head>
        <title>Vendrell Method: Fitness Program</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Container>
        <Box type="down">
          <div className={styles.form}>
            <Image
              className={styles.image}
              src="/logo.png"
              height={150}
              width={115}
              alt="Area Sports Logo"
            />
            <h2>Crear cuenta</h2>
            <TextInput
              value={firstName}
              onChange={(event) => setFirstName(event.target.value)}
              label="Nombre"
            />
            <TextInput
              value={lastName}
              onChange={(event) => setLastName(event.target.value)}
              label="Apellido"
            />
            <TextInput
              value={username}
              onChange={(event) => setUsername(event.target.value)}
              label="Nombre de usuario"
            />
            <TextInput
              value={email}
              onChange={(event) => setEmail(event.target.value)}
              label="Correo electronico"
              type="email"
            />
            <TextInput
              value={password}
              onChange={(event) => setPassword(event.target.value)}
              label="Contraseña"
              type="password"
            />
            <Button onClick={() => submitForm()}>Próximo</Button>
            <Footer />
          </div>
        </Box>
      </Container>
    </>
  );
}
