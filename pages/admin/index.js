import Head from "next/head";
import Image from "next/image";
import styles from "../../styles/admin/Home.module.scss";
import { useRouter } from "next/router";
import {
  Box,
  Container,
  Button,
  TextInput,
  Footer,
  Navbar,
} from "../../components";
import { useEffect, useState } from "react";

export default function Admin() {
  const router = useRouter();
  const [user, setUser] = useState();

  return (
    <>
      <Head>
        <title>Vendrell Method ADMIN: Home</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Navbar />
      <Container>
        <Box type="down">
          <div className={styles.header}>
            <h2>Dashboard</h2>
            <hr />
          </div>
          <div className={styles.grid}>
            <div className={styles.card}>
              <h2>Programas</h2>
              <p>
                Aquí podrás crear y editar programas. Añade las rutinas con los
                ejercicios disponibles.
              </p>
              <Button onClick={() => router.push("/admin/program")}>Ver</Button>
            </div>
            <div className={styles.card}>
              <h2>Rutinas</h2>
              <p>
                Aquí podrás crear y editar rutinas. Especifica en que consiste
                la rutina.
              </p>
              <Button onClick={() => router.push("/admin/routine")}>Ver</Button>
            </div>
            <div className={styles.card}>
              <h2>Ejercicios</h2>
              <p>
                Aquí podrás crear y editar ejercicios. Añade instrucciones y
                videos a cada ejercicio.
              </p>
              <Button onClick={() => router.push("/admin/exercises")}>
                Ver
              </Button>
            </div>
            <div className={styles.card}>
              <h2>Usuarios</h2>
              <p>
                Aquí podrás asignar programas de entrenamiento a los atletas.
              </p>
              <Button onClick={() => router.push("/admin/users")}>Ver</Button>
            </div>
            <div className={styles.card}>
              <h2>Actividades</h2>
              <p>
              Aquí podrás crear y editar actividades fisicas. Añade instrucciones y
                videos a cada actividad.
              </p>
              <Button onClick={() => router.push("/admin/activity")}>Ver</Button>
            </div>
          </div>
        </Box>
      </Container>
      <Footer />
    </>
  );
}
