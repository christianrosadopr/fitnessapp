import Head from "next/head";
import styles from "../../../styles/admin/program/Program.module.scss";
import {
  Box,
  Container,
  Button,
  Footer,
  Navbar,
} from "../../../components";
import { useRouter } from "next/router";

export async function getServerSideProps(context) {
  const res = await fetch(`${process.env.API}/programs`);
  const data = await res.json();

  if (!data) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      data,
    }, // will be passed to the page component as props
  };
}

export default function Program({ data }) {
  const router = useRouter();

  return (
    <>
      <Head>
        <title>Vendrell Method ADMIN: Program</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Navbar />
      <Container>
        <Box type="down">
          <div className={styles.taskbar}>
            <h2>Programas</h2>
            <div>
            <Button onClick={() => router.push("/admin")}>
              Volver
            </Button>
            <Button onClick={() => router.push("/admin/program/crear")}>
              Crear
            </Button>
            </div>
            
          </div>
          <div className={styles.grid}>
            {data.map((program, key) => (
              <div className={styles.card} key={key}>
                <h2 className={styles.header}>{program.programName}</h2>
                <p>Cantidad de rutinas: <strong>{program.routineList.length}</strong></p>
                <hr/>
                <p className={styles.rowdata}>
                  {program.description}
                </p>
                <div className={styles.buttonbox}>
                <Button onClick={() => router.push(`/admin/program/ver/${program.programID}`)}>Ver</Button>
                </div>
              </div>
            ))}
          </div>
        </Box>
      </Container>
      <Footer />
    </>
  );
}
