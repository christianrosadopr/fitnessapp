import Head from "next/head";
import styles from "../../../../styles/admin/program/Create.module.scss";
import {
  Box,
  Container,
  Button,
  TextInput,
  Footer,
  Navbar,
  TextArea,
} from "../../../../components";
import { useRouter } from "next/router";
import { useState } from "react";
import ReactSelect from "react-select";

export async function getServerSideProps(context) {
  const res = await fetch(`${process.env.API}/routine`);
  const data = await res.json();

  if (!data) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      data,
    }, // will be passed to the page component as props
  };
}

export default function CreateProgram({ data }) {
  const router = useRouter();
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [routineList, setRoutineList] = useState([]);

  async function submitForm() {
    await fetch(`https://258dv5hvol.execute-api.us-east-1.amazonaws.com/production/programs`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        programName: name,
        description: description,
        routineList: routineList,
        ownerID: 1
      }),
    })
      .then((res) => {
        res.json();
      })
      .catch((err) => console.log(err))
      .finally(() => router.back());
  }

  let options = [];
  data.forEach((routine) => {
    options.push({ value: routine.routineID, label: `${routine.day} - ${routine.routineName}` });
  });

  return (
    <>
      <Head>
        <title>Vendrell Method ADMIN: Crear Programa</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Navbar />
      <Container>
        <Box type="down">
          <div className={styles.taskbar}>
            <h2>Crear un Programa</h2>
          </div>
          <div className={styles.form}>
            <TextInput
              value={name}
              onChange={(event) => setName(event.target.value)}
              label="Nombre de programa"
            />
            <TextArea
              value={description}
              onChange={(event) => setDescription(event.target.value)}
              label="Descripción del programa"
            />
            <label>Rutinas</label>
            <ReactSelect
              className={styles.select}
              isMulti
              options={options}
              onChange={(value) => setRoutineList(value)}
            />
            <br />

            <Button onClick={() => submitForm()}>Crear</Button>
          </div>
        </Box>
      </Container>
      <Footer />
    </>
  );
}
