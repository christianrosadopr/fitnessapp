import Head from "next/head";
import styles from "../../../../styles/admin/program/View.module.scss";
import {
  Box,
  Container,
  Button,
  Footer,
  Navbar,
  TextArea,
  TextInput,
} from "../../../../components";
import { useRouter } from "next/router";
import { useState } from "react";
import { Modal } from "react-bootstrap";
import ReactSelect from "react-select";

export async function getServerSideProps(context) {
  const { id } = context.params;
  const res = await fetch(
    `${process.env.API}/programs/${id}`
  );
  const data = await res.json();

  if (!data) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      data,
    }, // will be passed to the page component as props
  };
}

export default function ViewProgram({ data }) {
  const router = useRouter();
  const [answer, setAnswer] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [showDescriptionModal, setShowDescriptionModal] = useState(false);
  const [showAddModal, setShowAddModal] = useState(false);
  const [showCreateModal, setShowCreateModal] = useState(false);
  const [description, setDescription] = useState(data.description);
  const [name, setName] = useState(data.programName);
  const [routineOptions, setRoutineOptions] = useState([]);
  const [newRoutines, setNewRoutines] = useState([]);
  // Name modal
  const [editNameModal, setEditNameModal] = useState(false);

  const { id } = router.query;

  // Delete Program
  async function deleteProgram() {
    await fetch(`https://258dv5hvol.execute-api.us-east-1.amazonaws.com/production/programs/${id}`, {
      method: "DELETE",
    })
      .then((res) => res.json())
      .then(() => router.back())
      .catch((err) => console.log(err));
  }

  // Edit Description
  async function editDescription() {
    await fetch(
      `https://258dv5hvol.execute-api.us-east-1.amazonaws.com/production/programs/editDescription/${id}`,
      {
        method: "PUT",
        body: JSON.stringify({ description: description }),
        headers: { "Content-Type": "application/json" },
      }
    )
      .then((res) => res.json())
      .then(() => {
        setShowDescriptionModal(false);
        router.reload();
      })
      .catch((err) => console.log(err));
  }

  // update Name
  async function updateName() {
    await fetch(`https://258dv5hvol.execute-api.us-east-1.amazonaws.com/production/programs/updateName/${id}`, {
      method: "PUT",
      body: JSON.stringify({ programName: name }),
      headers: { "Content-Type": "application/json" },
    })
      .then((res) => res.json())
      .then(() => router.reload())
      .catch((err) => console.log(err));
  }

  // Update Routines
  async function updateRoutines() {
    let tempList = data.routineList;
    tempList.push.apply(tempList, newRoutines);
    await fetch(
      `https://258dv5hvol.execute-api.us-east-1.amazonaws.com/production/programs/updateRoutineList/${id}`,
      {
        method: "PUT",
        body: JSON.stringify({ routineList: tempList }),
        headers: { "Content-Type": "application/json" },
      }
    )
      .then((res) => res.json())
      .then(() => router.reload())
      .catch((err) => console.log(err));
  }
  return (
    <>
      <Head>
        <title>Vendrell Method ADMIN: Program</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Navbar />
      {/* Modals */}
      {/* Delete Modal */}
      {showModal ? (
        <>
          <Modal show={showModal} onHide={() => setShowModal(false)} centered>
            <Modal.Body>
              ¿Estas seguro de que quieres eliminar este programa?
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={() => setShowModal(false)}>
                Salir
              </Button>
              <Button
                type="danger"
                onClick={async () => {
                  await deleteProgram();
                  setShowModal(false);
                  router.push("/admin/program");
                }}
              >
                Borrar
              </Button>
            </Modal.Footer>
          </Modal>
        </>
      ) : null}
      {/* Edit Description Modal */}
      {showDescriptionModal ? (
        <>
          <Modal
            show={showDescriptionModal}
            onHide={() => setShowDescriptionModal(false)}
            centered
          >
            <Modal.Header closeButton>
              <Modal.Title id="contained-modal-title-vcenter">
                Modifica la descripción
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <TextArea
                value={description}
                rows={5}
                cols={41}
                onChange={(event) => setDescription(event.target.value)}
              />
            </Modal.Body>
            <Modal.Footer>
              <Button
                onClick={async () => {
                  await editDescription();
                  setDescription("");
                  setShowDescriptionModal(false);
                }}
              >
                Guardar
              </Button>
              <Button
                type="secondary"
                onClick={async () => {
                  setShowDescriptionModal(false);
                }}
              >
                Salir
              </Button>
            </Modal.Footer>
          </Modal>
        </>
      ) : null}

      {/* Edit Name Modal */}
      {editNameModal ? (
        <>
          <Modal
            show={editNameModal}
            onHide={() => setEditNameModal(false)}
            centered
          >
            <Modal.Header closeButton>
              <Modal.Title id="contained-modal-title-vcenter">
                Cambia el nombre de la rutina
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <TextInput
                value={name}
                onChange={(event) => setName(event.target.value)}
                label="Nombre de ejercicio"
              />
            </Modal.Body>
            <Modal.Footer>
              <Button onClick={() => updateName()}>Actualizar</Button>
              <Button type="secondary" onClick={() => setShowModal(false)}>
                Salir
              </Button>
            </Modal.Footer>
          </Modal>
        </>
      ) : null}

      {/* Add Routine Modal */}
      {showAddModal ? (
        <>
          <Modal
            show={showAddModal}
            onHide={() => setShowAddModal(false)}
            centered
          >
            <Modal.Body>
              Añade rutina(s)
              <ReactSelect
                className={styles.select}
                isMulti
                options={routineOptions}
                onChange={(value) => setNewRoutines(value)}
              />
            </Modal.Body>
            <Modal.Footer>
              <Button
                onClick={async () => {
                  await updateRoutines();
                  setShowAddModal(false);
                  router.reload();
                }}
              >
                Añadir
              </Button>
              <Button
                variant="secondary"
                onClick={() => setShowAddModal(false)}
              >
                Salir
              </Button>
            </Modal.Footer>
          </Modal>
        </>
      ) : null}

      {/* Create Routine Modal */}
      {showCreateModal ? (
        <>
          <Modal
            show={showCreateModal}
            onHide={() => setShowCreateModal(false)}
            centered
          >
            <Modal.Body>Crea una rutina</Modal.Body>
            <Modal.Footer>
              <Button
                variant="secondary"
                onClick={() => setShowCreateModal(false)}
              >
                Salir
              </Button>
              <Button
                type="danger"
                onClick={async () => {
                  setShowCreateModal(false);
                  router.reload();
                }}
              >
                Borrar
              </Button>
            </Modal.Footer>
          </Modal>
        </>
      ) : null}

      <Container>
        <Box type="down">
          <div className={styles.taskbar}>
            <h2>Programas: {name}</h2>
            <div className={styles.btnbox}>
              <Button onClick={() => router.back()}>Volver</Button>
              <Button onClick={() => setEditNameModal(true)}>Editar</Button>
              <Button type="danger" onClick={() => setShowModal(true)}>
                Borrar
              </Button>
            </div>
          </div>
          <div className={styles.description}>
            <div className={styles.header}>
              <h2>Descripción</h2>
              <Button onClick={() => setShowDescriptionModal(true)}>
                Editar
              </Button>
            </div>

            <hr />
            <p>{data.description}</p>
          </div>
          <div className={styles.routines}>
            <div className={styles.header}>
              <h2>Rutinas</h2>
              <div className={styles.btnbox}>
                <Button
                  onClick={async () => {
                    await fetch(
                      `https://258dv5hvol.execute-api.us-east-1.amazonaws.com/production/routine`,
                      {
                        method: "GET",
                      }
                    )
                      .then((res) => res.json())
                      .then((data) => {
                        let options = [];
                        data.forEach((routine) => {
                          options.push({
                            value: routine.routineID,
                            label: `${routine.day} - ${routine.routineName}`,
                          });
                        });
                        setRoutineOptions(options);
                      })
                      .catch((err) => console.log(err));
                    setShowAddModal(true);
                  }}
                >
                  Añadir
                </Button>
                {/* <Button onClick={() => setShowCreateModal(true)}>Crear</Button> */}
              </div>
            </div>

            <hr />
            <div className={styles.grid}>
              {data.routineList.map((routine, key) => {
                let parsed = JSON.parse(routine);
                return (
                  <div
                    key={key}
                    className={styles.label}
                    onClick={() =>
                      router.push(`/admin/routine/ver/${parsed.value}`)
                    }
                  >
                    {parsed.label}
                  </div>
                );
              })}
            </div>
          </div>
        </Box>
      </Container>
      <Footer />
    </>
  );
}
