import Head from "next/head";
import styles from "../../../styles/admin/program/Program.module.scss";
import { Box, Container, Button, Footer, Navbar } from "../../../components";
import { useRouter } from "next/router";

export async function getServerSideProps(context) {
  const res = await fetch(`${process.env.API}/activity`);
  const data = await res.json();

  if (!data) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      data,
    }, // will be passed to the page component as props
  };
}

export default function Activity({ data }) {
  const router = useRouter();

  return (
    <>
      <Head>
        <title>Vendrell Method ADMIN: Actividades</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Navbar />
      <Container>
        <Box type="down">
          <div className={styles.taskbar}>
            <h2>Actividades</h2>
            <div>
              <Button onClick={() => router.push("/admin")}>Volver</Button>
              <Button onClick={() => router.push("/admin/activity/crear")}>
                Crear
              </Button>
            </div>
          </div>
          <div className={styles.grid}>
            {data.map((activity, key) => (
              <div className={styles.card} key={key}>
                <h2 className={styles.header}>{activity.activityName}</h2>
                <p>
                  Categoria de actividad: <strong>{activity.tag}</strong>
                  <br />
                  Cantidad de repeticiones: <strong>{activity.reps}</strong>
                  <br />
                  Cantidad de sets: <strong>{activity.sets}</strong>
                  <br />
                  Descanso entre set: <strong>{activity.rest}</strong>
                </p>

                <hr />
                <p className={styles.rowdata}>{activity.description}</p>
                <div className={styles.buttonbox}>
                  <Button
                    onClick={() =>
                      router.push(`/admin/activity/ver/${activity.activityID}`)
                    }
                  >
                    Ver
                  </Button>
                </div>
              </div>
            ))}
          </div>
        </Box>
      </Container>
      <Footer />
    </>
  );
}
