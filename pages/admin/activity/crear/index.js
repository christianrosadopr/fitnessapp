import Head from "next/head";
import styles from "../../../../styles/admin/program/Create.module.scss";
import {
  Box,
  Container,
  Button,
  TextInput,
  Footer,
  Navbar,
  TextArea,
  Select,
} from "../../../../components";
import { useRouter } from "next/router";
import { useState } from "react";

export async function getServerSideProps(context) {
  const res = await fetch(
    `${process.env.API}/activity/findTags`
  );
  const data = await res.json();

  if (!data) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      data,
    }, // will be passed to the page component as props
  };
}

export default function CreateActivity({ data }) {
  const router = useRouter();
  const [name, setName] = useState("");
  const [instructions, setInstructions] = useState("");
  const [reps, setReps] = useState(0);
  const [sets, setSets] = useState(0);
  const [rest, setRest] = useState(0);
  const [videoURL, setVideoURL] = useState("");
  const [tag, setTag] = useState("")
  const [loading, setLoading] = useState(false)

  async function submitForm() {
    setLoading(true);
    let videoID = 'TBD';
    if (videoID.length > 0) {
      videoID = videoURL.split("v=")[1];
    }
    
    await fetch(`https://258dv5hvol.execute-api.us-east-1.amazonaws.com/production/activity`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        activityName: name,
        description: instructions,
        reps: reps,
        sets: sets,
        rest: rest,
        videoURL: videoID,
        tag: tag
      }),
    })
      .then((res) => {
        res.json();
        setLoading(false)
      })
      .catch((err) => console.log(err))
      .finally(() => router.back());
  }

  return (
    <>
      <Head>
        <title>Vendrell Method ADMIN: Crear Actividad</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Navbar />
      <Container>
        <Box type="down">
          <div className={styles.taskbar}>
            <h2>Crear una actividad</h2>
          </div>
          <div className={styles.form}>
            <TextInput
              value={name}
              onChange={(event) => setName(event.target.value)}
              label="Nombre de actividad"
            />
            <TextArea
              value={instructions}
              onChange={(event) => setInstructions(event.target.value)}
              label="Instrucciones"
            />
            <TextInput
              value={reps}
              onChange={(event) => setReps(event.target.value)}
              label="Repeticiones"
              type="number"
            />
            <TextInput
              value={sets}
              onChange={(event) => setSets(event.target.value)}
              label="Sets"
              type="number"
            />
            <TextInput
              value={videoURL}
              onChange={(event) => setVideoURL(event.target.value)}
              label="Video de explicación"
              type="text"
            />
            <Select
              label="Descanso"
              value={rest}
              onChange={(event) => setRest(event.target.value)}
              options={[
                "5 segundos",
                "10 segundos",
                "15 segundos",
                "30 segundos",
                "1 minuto",
                "1:30 minuto",
              ]}
            />
            <TextInput
              value={tag}
              onChange={(event) => setTag(event.target.value.toUpperCase())}
              label="Tag"
              type="text"
            />
          
            <Button onClick={() => submitForm()}>Crear</Button>
          </div>
        </Box>
      </Container>
      <Footer />
    </>
  );
}
