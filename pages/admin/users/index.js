import Head from "next/head";
import Image from "next/image";
import styles from "../../../styles/admin/program/Program.module.scss";
import {
  Box,
  Container,
  Button,
  Navbar,
  Footer,
  Select,
} from "../../../components";
import { useRouter } from "next/router";
import { useState } from "react";
import { Modal } from "react-bootstrap";

export async function getServerSideProps(context) {
  const res = await fetch(`${process.env.API}/users`);
  const programRes = await fetch(`${process.env.API}/programs`);
  const data = await res.json();
  const programData = await programRes.json();

  if (!data) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      data,
      programData,
    }, // will be passed to the page component as props
  };
}

export default function Admin({ data, programData }) {
  const router = useRouter();
  const [showEditUserModal, setShowEditUserModal] = useState(false);
  const [showDeleteUserModal, setShowDeleteUserModal] = useState(false);
  const [trainingProgram, setTrainingProgram] = useState(0);
  const [selectedUser, setSelectedUser] = useState();
  const [selectedOption, setSelectedOption] = useState("")

  async function editUser() {
    await fetch(`https://258dv5hvol.execute-api.us-east-1.amazonaws.com/production/users/updateProgram/${selectedUser}`, {
      method: "PUT",
      body: JSON.stringify({ trainingProgram: trainingProgram }),
      headers: { "Content-Type": "application/json" },
    })
      .then((res) => res.json())
      .then(() => {
        setShowEditUserModal(false);
        setTrainingProgram(0);
        router.reload();
      })
      .catch((err) => console.log(err));
  }

  async function deleteUser() {
    await fetch(`https://258dv5hvol.execute-api.us-east-1.amazonaws.com/production/users/${selectedUser}`, {
      method: "DELETE",
    })
      .then((res) => res.json())
      .then(() => {
        setShowDeleteUserModal(false);
        router.reload();
      })
      .catch((err) => console.log(err));
  }

  let optionList = []
  programData.forEach(program => {
    optionList.push(`${program.programID} - ${program.programName}`)
  });

  return (
    <>
      <Head>
        <title>Vendrell Method ADMIN: Users</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Navbar />
      {/* Delete Modal */}
      {showDeleteUserModal ? (
        <>
          <Modal
            show={showDeleteUserModal}
            onHide={() => setShowDeleteUserModal(false)}
            centered
          >
            <Modal.Body>
              ¿Estas seguro de que quieres eliminar este usuario?
            </Modal.Body>
            <Modal.Footer>
              <Button
                variant="secondary"
                onClick={() => setShowDeleteUserModal(false)}
              >
                Salir
              </Button>
              <Button
                type="danger"
                onClick={async () => {
                  await deleteUser();
                }}
              >
                Borrar
              </Button>
            </Modal.Footer>
          </Modal>
        </>
      ) : null}

      {/* Edit User Modal */}
      {showEditUserModal ? (
        <>
          <Modal
            show={showEditUserModal}
            onHide={() => setShowEditUserModal(false)}
            centered
          >
            <Modal.Header>
              Escoja el programa de entrenamiento para el usuario
            </Modal.Header>
            <Modal.Body>
              <Select
                value={selectedOption}
                onChange={(event) => {
                  let temp = event.target.value;
                  setSelectedOption(temp)
                  setTrainingProgram(parseInt((temp.split('-')[0])))
                }}
                options={optionList}
                label="Programas de Entrenamiento"
              />
            </Modal.Body>
            <Modal.Footer>
              <Button
                onClick={async () => {
                  await editUser();
                }}
              >
                Guardar
              </Button>
              <Button
                variant="secondary"
                onClick={() => setShowEditUserModal(false)}
              >
                Salir
              </Button>
            </Modal.Footer>
          </Modal>
        </>
      ) : null}

      <Container>
        <Box type="down">
          <div className={styles.body}>
            <div className={styles.taskbar}>
              <h2>Usuarios</h2>
              {/* <Button onClick={() => router.push("/admin/routine/crear")}>
              Crear
            </Button> */}
            </div>
            <div className={styles.grid}>
              {data.map((user, key) => (
                <div className={styles.usercard} key={key}>
                  <h2 className={styles.header}>
                    {user.firstName} {user.lastName}
                  </h2>
                  <p>
                    Programa de entrenamiento:{" "}
                    <strong>{user.trainingProgram}</strong>
                    <br />
                  </p>
                  <hr />
                  <div className={styles.buttonbox}>
                    <Button
                      onClick={async () => {
                        setSelectedUser(user.userID);
                        setShowEditUserModal(true);
                      }}
                    >
                      Editar
                    </Button>
                    <Button
                      onClick={() => {
                        setSelectedUser(user.userID);
                        setShowDeleteUserModal(true);
                      }}
                    >
                      Borrar
                    </Button>
                    {/* <Button onClick={() => router.push(`/admin/routine/editar/${routine.routineID}`)}>Editar</Button> */}
                  </div>
                </div>
              ))}
            </div>
          </div>
        </Box>
      </Container>
      <Footer />
    </>
  );
}
