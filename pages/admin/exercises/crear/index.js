import Head from "next/head";
import styles from "../../../../styles/admin/program/Create.module.scss";
import {
  Box,
  Container,
  Button,
  TextInput,
  Footer,
  Navbar,
  TextArea,
  Select,
} from "../../../../components";
import { useRouter } from "next/router";
import { useState } from "react";


export default function CreateExercise() {
  const router = useRouter();
  const [name, setName] = useState("");
  const [instructions, setInstructions] = useState("");
  const [reps, setReps] = useState(0);
  const [sets, setSets] = useState(0);
  const [rest, setRest] = useState(0);
  const [videoURL, setVideoURL] = useState("");

  async function submitForm() {
    let videoID = 'TBD';
    if (videoID.length > 0) {
      videoID = videoURL.split("v=")[1];
    }
    await fetch(`https://258dv5hvol.execute-api.us-east-1.amazonaws.com/production/exercises`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        exerciseName: name,
        description: instructions,
        reps: reps,
        sets: sets,
        rest: rest,
        videoURL: videoID,
      }),
    })
      .then((res) => {
        res.json();
      })
      .catch((err) => console.log(err))
      .finally(() => router.back());
  }

  return (
    <>
      <Head>
        <title>Vendrell Method ADMIN: Crear Ejercicios</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Navbar />
      <Container>
        <Box type="down">
          <div className={styles.taskbar}>
            <h2>Crear un Ejercicios</h2>
          </div>
          <div className={styles.form}>
            <TextInput
              value={name}
              onChange={(event) => setName(event.target.value)}
              label="Nombre de ejercicio"
            />
            <TextArea
              value={instructions}
              onChange={(event) => setInstructions(event.target.value)}
              label="Instrucciones"
            />
            <TextInput
              value={reps}
              onChange={(event) => setReps(event.target.value)}
              label="Repeticiones"
              type="number"
            />
            <TextInput
              value={sets}
              onChange={(event) => setSets(event.target.value)}
              label="Sets"
              type="number"
            />
            <TextInput
              value={videoURL}
              onChange={(event) => setVideoURL(event.target.value)}
              label="Video de explicación"
              type="text"
            />
            <Select
              label="Descanso"
              value={rest}
              onChange={(event) => setRest(event.target.value)}
              options={[
                "5 segundos",
                "10 segundos",
                "15 segundos",
                "30 segundos",
                "1 minuto",
                "1:30 minuto",
              ]}
            />
            <Button onClick={() => submitForm()}>Crear</Button>
          </div>
        </Box>
      </Container>
      <Footer />
    </>
  );
}
