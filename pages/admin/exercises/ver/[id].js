import { useState } from "react";
import Head from "next/head";
import Image from "next/image";
import styles from "../../../../styles/admin/program/View.module.scss";
import {
  Box,
  Container,
  Button,
  Navbar,
  Footer,
  TextInput,
  TextArea,
  Select,
} from "../../../../components";
import { Modal } from "react-bootstrap";
import { useRouter } from "next/router";

export async function getServerSideProps(context) {
  const { id } = context.params;
  const res = await fetch(`${process.env.API}/exercises/${id}`);
  const data = await res.json();

  if (!data) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      data,
    }, // will be passed to the page component as props
  };
}

export default function ViewExercise({ data }) {
  const router = useRouter();

  const [name, setName] = useState(data.exerciseName);
  const [description, setDescription] = useState(data.description);
  const [reps, setReps] = useState(data.reps);
  const [sets, setSets] = useState(data.sets);
  const [rest, setRest] = useState(data.rest);
  const [videoURL, setVideoURL] = useState(
    `https://www.youtube.com/embed/${data.videoURL}`
  );

  const [showDescriptionModal, setShowDescriptionModal] = useState(false);
  const [showInformationModal, setShowInformationModal] = useState(false);
  const [editNameModal, setEditNameModal] = useState(false);

  const { id } = router.query;
  // Modal form functions
  // Edit Description
  async function editDescription() {
    await fetch(
      `https://258dv5hvol.execute-api.us-east-1.amazonaws.com/production/exercises/updateInstructions/${id}`,
      {
        method: "PUT",
        body: JSON.stringify({ description: description }),
        headers: { "Content-Type": "application/json" },
      }
    )
      .then((res) => res.json())
      .then(() => setShowDescriptionModal(false))
      .catch((err) => console.log(err));
  }

  // Edit Information
  async function editInformation() {
    let videoID = 'TBD';
    if (videoID.length > 0) {
      videoID = videoURL.split("v=")[1];
    }
    await fetch(
      `https://258dv5hvol.execute-api.us-east-1.amazonaws.com/production/exercises/${id}`,
      {
        method: "PUT",
        body: JSON.stringify({
          exerciseName: name,
          description: description,
          videoURL: videoID,
          reps: reps,
          sets: parseInt(sets),
          rest: rest,
        }),
        headers: { "Content-Type": "application/json" },
      }
    )
      .then((res) => res.json())
      .then(() => {
        setShowInformationModal(false);
        router.reload();
      })
      .catch((err) => console.log(err));
  }

  // Delete Exercise
  async function deleteExercise() {
    await fetch(
      `https://258dv5hvol.execute-api.us-east-1.amazonaws.com/production/exercises/${id}`,
      {
        method: "DELETE",
      }
    )
      .then((res) => res.json())
      .then(() => router.back())
      .catch((err) => console.log(err));
  }

  // update Name
  async function updateName() {
    await fetch(
      `https://258dv5hvol.execute-api.us-east-1.amazonaws.com/production/exercises/updateName/${id}`,
      {
        method: "PUT",
        body: JSON.stringify({ exerciseName: name }),
        headers: { "Content-Type": "application/json" },
      }
    )
      .then((res) => res.json())
      .then(() => router.reload())
      .catch((err) => console.log(err));
  }

  return (
    <>
      <Head>
        <title>Vendrell Method ADMIN: Exercises</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Navbar />

      {/* Edit Description Modal */}
      {showDescriptionModal ? (
        <>
          <Modal
            show={showDescriptionModal}
            onHide={() => setShowDescriptionModal(false)}
            centered
          >
            <Modal.Header closeButton>
              <Modal.Title id="contained-modal-title-vcenter">
                Modifica las instrucciones
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <TextArea
                value={description}
                rows={5}
                cols={41}
                onChange={(event) => setDescription(event.target.value)}
              />
            </Modal.Body>
            <Modal.Footer>
              <Button
                onClick={async () => {
                  await editDescription();
                  // setDescription("");
                  setShowDescriptionModal(false);
                  // router.reload();
                }}
              >
                Guardar
              </Button>
              <Button
                type="secondary"
                onClick={async () => {
                  setShowDescriptionModal(false);
                }}
              >
                Salir
              </Button>
            </Modal.Footer>
          </Modal>
        </>
      ) : null}

      {/* Edit Information Modal */}
      {showInformationModal ? (
        <>
          <Modal
            show={showInformationModal}
            onHide={() => setShowInformationModal(false)}
            centered
          >
            <Modal.Header closeButton>
              <Modal.Title id="contained-modal-title-vcenter">
                Modifica la informacion del ejercicio
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <TextInput
                value={reps}
                onChange={(event) => setReps(event.target.value)}
                label="Repeticiones"
                type="number"
              />
              <TextInput
                value={sets}
                onChange={(event) => setSets(event.target.value)}
                label="Sets"
                type="number"
              />
              <TextInput
                value={videoURL}
                onChange={(event) => setVideoURL(event.target.value)}
                label="Video de explicación"
                type="text"
              />
              <Select
                label="Descanso"
                value={rest}
                onChange={(event) => setRest(event.target.value)}
                options={[
                  "5 segundos",
                  "10 segundos",
                  "15 segundos",
                  "30 segundos",
                  "1 minuto",
                  "1:30 minuto",
                ]}
              />
            </Modal.Body>
            <Modal.Footer>
              <Button onClick={() => editInformation()}>Actualizar</Button>
              <Button
                type="secondary"
                onClick={() => setShowInformationModal(false)}
              >
                Salir
              </Button>
            </Modal.Footer>
          </Modal>
        </>
      ) : null}

      {/* Edit Name Modal */}
      {editNameModal ? (
        <>
          <Modal
            show={editNameModal}
            onHide={() => setEditNameModal(false)}
            centered
          >
            <Modal.Header closeButton>
              <Modal.Title id="contained-modal-title-vcenter">
                Cambia el nombre de la rutina
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <TextInput
                value={name}
                onChange={(event) => setName(event.target.value)}
                label="Nombre de ejercicio"
              />
            </Modal.Body>
            <Modal.Footer>
              <Button onClick={() => updateName()}>Actualizar</Button>
              <Button type="secondary" onClick={() => setShowModal(false)}>
                Salir
              </Button>
            </Modal.Footer>
          </Modal>
        </>
      ) : null}

      <Container>
        <Box type="down">
          <div className={styles.taskbar}>
            <h2>Ejercicio: {data.exerciseName}</h2>
            <div className={styles.btnbox}>
              <Button onClick={() => router.back()}>Volver</Button>
              <Button onClick={() => setEditNameModal(true)}>Editar</Button>
              <Button type="danger" onClick={() => deleteExercise()}>
                Borrar
              </Button>
            </div>
          </div>
          {/* Description */}
          <div className={styles.description}>
            <div className={styles.header}>
              <h2>Instrucciones</h2>
              <Button onClick={() => setShowDescriptionModal(true)}>
                Editar
              </Button>
            </div>

            <hr />
            <p>{data.description}</p>
          </div>
          {/* Information with video preview */}
          <div className={styles.description}>
            <div className={styles.header}>
              <h2>Informacion</h2>
              <Button onClick={() => setShowInformationModal(true)}>
                Editar
              </Button>
            </div>
            <hr />
            <p>
              Cantidad de repeticiones: <strong>{data.reps}</strong>
              <br />
              Cantidad de sets: <strong>{data.sets}</strong>
              <br />
              Descanso entre ejercicio: <strong>{data.rest}</strong>
            </p>

            {data.videoURL === "TBD" ? (
              <>
              <h4>Previa del video</h4>
              <TextInput
                value={videoURL}
                onChange={(event) => setVideoURL(event.target.value)}
                label="Añadir video"
                type="text"
              />
              <Button onClick={() => editDescription()}>Guardar</Button>
              </>
            ) : (
              <>
                <h4>Previa del video</h4>
                <iframe
                  width="350"
                  height="200"
                  src={`https://www.youtube.com/embed/${data.videoURL}`}
                  frameBorder="0"
                  allowFullScreen
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                ></iframe>
              </>
            )}
          </div>
        </Box>
      </Container>
      <Footer />
    </>
  );
}
