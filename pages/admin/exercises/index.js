import Head from "next/head";
import styles from "../../../styles/admin/program/Program.module.scss";
import { Box, Container, Button, Footer, Navbar } from "../../../components";
import { useRouter } from "next/router";

export async function getServerSideProps(context) {
  const res = await fetch(`${process.env.API}/exercises`);
  const data = await res.json();

  if (!data) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      data,
    }, // will be passed to the page component as props
  };
}

export default function Exercise({ data }) {
  const router = useRouter();

  return (
    <>
      <Head>
        <title>Vendrell Method ADMIN: Ejercicios</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Navbar />
      <Container>
        <Box type="down">
          <div className={styles.taskbar}>
            <h2>Ejercicios</h2>
            <div>
              <Button onClick={() => router.push("/admin")}>Volver</Button>
              <Button onClick={() => router.push("/admin/exercises/crear")}>
                Crear
              </Button>
            </div>
          </div>
          <div className={styles.grid}>
            {data.map((exercise, key) => (
              <div className={styles.card} key={key}>
                <h2 className={styles.header}>{exercise.exerciseName}</h2>
                <p>
                  Cantidad de repeticiones: <strong>{exercise.reps}</strong>
                  <br />
                  Cantidad de sets: <strong>{exercise.sets}</strong>
                  <br />
                  Descanso entre ejercicio: <strong>{exercise.rest}</strong>
                </p>

                <hr />
                <p className={styles.rowdata}>{exercise.description}</p>
                <div className={styles.buttonbox}>
                  <Button
                    onClick={() =>
                      router.push(`/admin/exercises/ver/${exercise.exerciseID}`)
                    }
                  >
                    Ver
                  </Button>
                </div>
              </div>
            ))}
          </div>
        </Box>
      </Container>
      <Footer />
    </>
  );
}
