import Head from "next/head";
import styles from "../../../styles/admin/program/Program.module.scss";
import {
  Box,
  Container,
  Button,
  TextInput,
  Footer,
  Navbar,
} from "../../../components";
import { useRouter } from "next/router";

export async function getServerSideProps(context) {
  const res = await fetch(`${process.env.API}/routine`);
  const data = await res.json();

  if (!data) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      data,
    }, // will be passed to the page component as props
  };
}

export default function Routine({ data }) {
  const router = useRouter();

  return (
    <>
      <Head>
        <title>Vendrell Method ADMIN: Rutinas</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Navbar />
      <Container>
        <Box type="down">
          <div className={styles.taskbar}>
            <h2>Rutinas</h2>
            <div>
            <Button onClick={() => router.push("/admin")}>
              Volver
            </Button>
            <Button onClick={() => router.push("/admin/routine/crear")}>
              Crear
            </Button>
            </div>
            
          </div>
          <div className={styles.grid}>
            {data.map((routine, key) => (
              <div className={styles.card} key={key}>
                <h2 className={styles.header}>{routine.routineName}</h2>
                <p>
                  Cantidad de calentamientos: <strong>{routine.warmupList.length}</strong><br/>
                  Cantidad de ejercicios: <strong>{routine.exerciseList.length}</strong><br/>
                  Cantidad de actividades: <strong>{routine.activityList.length}</strong>
                </p>
                <hr/>
                <p className={styles.rowdata}>
                  {routine.instructions}
                </p>
                <div className={styles.buttonbox}>
                <Button onClick={() => router.push(`/admin/routine/ver/${routine.routineID}`)}>Ver</Button>
                {/* <Button onClick={() => router.push(`/admin/routine/editar/${routine.routineID}`)}>Editar</Button> */}
                </div>
              </div>
            ))}
          </div>
        </Box>
      </Container>
      <Footer />
    </>
  );
}
