import Head from "next/head";
import styles from "../../../../styles/admin/program/View.module.scss";
import {
  Box,
  TextInput,
  Container,
  Button,
  Footer,
  Select,
  TextArea,
  Navbar,
} from "../../../../components";
import { useRouter } from "next/router";
import { useState } from "react";
import { Modal } from "react-bootstrap";
import ReactSelect from "react-select";

export async function getServerSideProps(context) {
  const { id } = context.params;
  // fetch rutinas
  const res = await fetch(`${process.env.API}/routine/${id}`);
  const data = await res.json();
  // fetch ejercicios
  const res2 = await fetch(`${process.env.API}/exercises`);
  const exerciseData = await res2.json();
  // Fetch actividades
  const res3 = await fetch(`${process.env.API}/activity`);
  const activityData = await res3.json();

  if (!data) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      data,
      exerciseData,
      activityData,
    }, // will be passed to the page component as props
  };
}

export default function ViewRoutine({ data, exerciseData, activityData }) {
  const router = useRouter();
  const { id } = router.query;
  const [type, setType] = useState("");
  const [showModal, setShowModal] = useState(false);

  // Create Modal
  const [showCreateModal, setShowCreateModal] = useState(false);
  const [showCreateActivityModal, setShowCreateActivityModal] = useState(false);

  const [routineName, setRoutineName] = useState(data.routineName)
  const [name, setName] = useState("");
  const [instructions, setInstructions] = useState("");
  const [reps, setReps] = useState(0);
  const [sets, setSets] = useState(0);
  const [rest, setRest] = useState("");
  const [videoURL, setVideoURL] = useState("");
  const [tag, setTag] = useState("");
  const [day, setDay] = useState(data.day);

  // New
  const [newExercises, setNewExercises] = useState([]);
  const [newWarmups, setNewWarmups] = useState([]);
  const [newActivities, setNewActivities] = useState([]);

  // Name modal
  const [editNameModal, setEditNameModal] = useState(false);

  // Add Modal
  const [showAddModal, setShowAddModal] = useState(false);
  const [showAddActivityModal, setShowAddActivityModal] = useState(false);
  // Show Instruction Modal
  const [showDescriptionModal, setShowDescriptionModal] = useState(false);
  const [description, setDescription] = useState(data.instructions);

  // Delete modal
  const [showDeleteExercise, setShowDeleteExercise] = useState(false);

  // Selected Item
  const [selectedItem, setSelectedItem] = useState();

  // Day Modal
  const [showDayModal, setShowDayModal] = useState(false);

  // Edit Workout Day
  async function editWorkoutDay() {
    console.log(description);
    await fetch(
      `https://258dv5hvol.execute-api.us-east-1.amazonaws.com/production/routine/updateDay/${id}`,
      {
        method: "PUT",
        body: JSON.stringify({ day: day }),
        headers: { "Content-Type": "application/json" },
      }
    )
      .then((res) => res.json())
      .then(() => {
        showDescriptionModal(false);
        router.reload();
      })
      .catch((err) => console.log(err));
  }

  // Edit Description
  async function editDescription() {
    console.log(description);
    await fetch(
      `https://258dv5hvol.execute-api.us-east-1.amazonaws.com/production/routine/updateInstructions/${id}`,
      {
        method: "PUT",
        body: JSON.stringify({ instructions: description }),
        headers: { "Content-Type": "application/json" },
      }
    )
      .then((res) => res.json())
      .then(() => {
        showDescriptionModal(false);
        router.reload();
      })
      .catch((err) => console.log(err));
  }

  // Crear ejercicio
  async function submitForm() {
    let videoID = "TBD";
    if (videoID.length > 0) {
      videoID = videoURL.split("v=")[1];
    }
    await fetch(
      `https://258dv5hvol.execute-api.us-east-1.amazonaws.com/production/exercises`,
      {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          exerciseName: name,
          description: instructions,
          reps: reps,
          sets: sets,
          rest: rest,
          videoURL: videoID,
        }),
      }
    )
      .then((res) => {
        res.json();
      })
      .then(async (data) => {
        const { routineID, exerciseName } = data;
        let tempList = [];
        if (type === "exercise") {
          tempList = data.exerciseList;
          tempList.push({ value: routineID, label: exerciseName });
          await fetch(
            `https://258dv5hvol.execute-api.us-east-1.amazonaws.com/production/routine/updateExerciseList/${routineID}`,
            {
              method: "PUT",
              body: JSON.stringify({ exerciseList: tempList }),
              headers: { "Content-Type": "application/json" },
            }
          )
            .then((res) => res.json())
            .catch((err) => console.log(err));
        } else {
          tempList = data.warmupList;
          tempList.push({ value: routineID, label: exerciseName });
          await fetch(
            `https://258dv5hvol.execute-api.us-east-1.amazonaws.com/production/routine/updateWarmupList/${routineID}`,
            {
              method: "PUT",
              body: JSON.stringify({ warmupList: tempList }),
              headers: { "Content-Type": "application/json" },
            }
          )
            .then((res) => res.json())
            .catch((err) => console.log(err));
        }

        setInstructions("");
        setName("");
        setReps(0);
        setSets(0);
        setRest("");
        setVideoURL("");
        setShowCreateModal(false);
      })
      .catch((err) => console.log(err))
      .finally(() => router.back());
  }

  // añadir Activity
  async function submitActivity() {
    let videoID = "TBD";
    if (videoID.length > 0) {
      videoID = videoURL.split("v=")[1];
    }
    await fetch(
      `https://258dv5hvol.execute-api.us-east-1.amazonaws.com/production/activity`,
      {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          activityName: name,
          description: instructions,
          reps: reps,
          sets: sets,
          rest: rest,
          videoURL: videoID,
          tag: tag,
        }),
      }
    )
      .then((res) => {
        res.json();
        setInstructions("");
        setName("");
        setReps(0);
        setSets(0);
        setRest("");
        setVideoURL("");
        setShowCreateModal(false);
      })
      .catch((err) => console.log(err))
      .finally(() => console.log("DONE"));
  }

  // borrar Rutina
  async function deleteRoutine() {
    await fetch(
      `https://258dv5hvol.execute-api.us-east-1.amazonaws.com/production/routine/${id}`,
      {
        method: "DELETE",
      }
    )
      .then((res) => res.json())
      .then(() => router.back())
      .catch((err) => console.log(err));
  }

  // update Name
  async function updateName() {
    await fetch(
      `https://258dv5hvol.execute-api.us-east-1.amazonaws.com/production/routine/updateName/${id}`,
      {
        method: "PUT",
        body: JSON.stringify({ routineName: routineName }),
        headers: { "Content-Type": "application/json" },
      }
    )
      .then((res) => res.json())
      .catch((err) => console.log(err));

    router.reload();
  }

  let options = [];
  exerciseData.forEach((exercise) => {
    options.push({ value: exercise.exerciseID, label: exercise.exerciseName });
  });

  let activityOptions = [];
  activityData.forEach((activity) => {
    activityOptions.push({
      value: activity.activityID,
      label: `${activity.activityName} - ${activity.tag}`,
    });
  });

  async function deleteExercise(type) {
    let tempList = [];
    // Verify type and find exercise/warmup to delete
    if (type === "calentamiento") {
      tempList = data.warmupList;
    } else {
      tempList = data.exerciseList;
    }
    const index = tempList.indexOf(JSON.stringify(selectedItem));
    if (index > -1) {
      tempList.splice(index, 1);
    }
    // updating in the database
    if (type === "calentamiento") {
      tempList = data.warmupList;
      await fetch(
        `https://258dv5hvol.execute-api.us-east-1.amazonaws.com/production/routine/updateWarmupList/${id}`,
        {
          method: "PUT",
          body: JSON.stringify({ warmupList: tempList }),
          headers: { "Content-Type": "application/json" },
        }
      )
        .then((res) => res.json())
        .then(() => router.reload())
        .catch((err) => console.log(err));
    } else {
      tempList = data.exerciseList;
      await fetch(
        `https://258dv5hvol.execute-api.us-east-1.amazonaws.com/production/routine/updateExerciseList/${id}`,
        {
          method: "PUT",
          body: JSON.stringify({ exerciseList: tempList }),
          headers: { "Content-Type": "application/json" },
        }
      )
        .then((res) => res.json())
        .then(() => router.reload())
        .catch((err) => console.log(err));
    }

    setShowDeleteExercise(false);
  }

  return (
    <>
      <Head>
        <title>Vendrell Method ADMIN: Rutina</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Navbar />
      {/* Delete Modal */}
      {showModal ? (
        <>
          <Modal show={showModal} onHide={() => setShowModal(false)} centered>
            <Modal.Body>
              Estas seguro de que quieres eliminar esta rutina?
            </Modal.Body>
            <Modal.Footer>
              <Button
                type="danger"
                onClick={() => {
                  deleteRoutine();
                  setShowModal(false);
                  router.back();
                }}
              >
                Borrar
              </Button>
              <Button variant="secondary" onClick={() => setShowModal(false)}>
                Salir
              </Button>
            </Modal.Footer>
          </Modal>
        </>
      ) : null}

      {/* Show Day Modal */}
      {showDayModal ? (
        <>
          <Modal
            show={showDayModal}
            onHide={() => setShowDayModal(false)}
            centered
          >
            <Modal.Header closeButton>
              <Modal.Title id="contained-modal-title-vcenter">
                Modifica dia de Entrenamiento
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <input
                type="number"
                value={day}
                onChange={(event) => setDay(event.target.value)}
                min="1"
                max="999"
              />
            </Modal.Body>
            <Modal.Footer>
              <Button
                onClick={() => {
                  editWorkoutDay();
                  setShowModal(false);
                  router.back();
                }}
              >
                Guardar
              </Button>
              <Button type="danger" onClick={() => setShowModal(false)}>
                Salir
              </Button>
            </Modal.Footer>
          </Modal>
        </>
      ) : null}

      {/* Delete Exercise Modal */}
      {showDeleteExercise ? (
        <>
          <Modal
            show={showDeleteExercise}
            onHide={() => setShowDeleteExercise(false)}
            centered
          >
            <Modal.Body>
              Estas seguro de que quieres eliminar este {type}:
              {selectedItem.label}?
            </Modal.Body>
            <Modal.Footer>
              <Button
                type="danger"
                onClick={() => {
                  deleteExercise(type);
                  setShowModal(false);
                  // router.reload();
                }}
              >
                Borrar
              </Button>
              <Button
                variant="secondary"
                onClick={() => setShowDeleteExercise(false)}
              >
                Salir
              </Button>
            </Modal.Footer>
          </Modal>
        </>
      ) : null}

      {/* Edit Description Modal */}
      {showDescriptionModal ? (
        <>
          <Modal
            show={showDescriptionModal}
            onHide={() => setShowDescriptionModal(false)}
            centered
          >
            <Modal.Header closeButton>
              <Modal.Title id="contained-modal-title-vcenter">
                Modifica las instrucciones
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <TextArea
                value={description}
                rows={5}
                cols={41}
                onChange={(event) => setDescription(event.target.value)}
              />
            </Modal.Body>
            <Modal.Footer>
              <Button
                onClick={async () => {
                  await editDescription();
                  // setDescription("");
                  setShowDescriptionModal(false);
                  // router.reload();
                }}
              >
                Guardar
              </Button>
              <Button
                type="secondary"
                onClick={async () => {
                  setShowDescriptionModal(false);
                }}
              >
                Salir
              </Button>
            </Modal.Footer>
          </Modal>
        </>
      ) : null}

      {/* Add Modal */}
      {showAddModal ? (
        <>
          <Modal
            show={showAddModal}
            onHide={() => setShowAddModal(false)}
            centered
          >
            <Modal.Header closeButton>
              <Modal.Title id="contained-modal-title-vcenter">
                Añade un {type === "exercise" ? "Ejercicio" : "Calentamiento"}
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <ReactSelect
                isMulti
                className={styles.select}
                options={options}
                onChange={(value) => {
                  if (type === "exercise") {
                    setNewExercises(value);
                  } else {
                    setNewWarmups(value);
                  }
                }}
              />
            </Modal.Body>
            <Modal.Footer>
              <Button
                variant="secondary"
                onClick={async () => {
                  let tempList = [];
                  if (type === "exercise") {
                    tempList = data.exerciseList;
                    tempList.push.apply(tempList, newExercises);
                    await fetch(
                      `https://258dv5hvol.execute-api.us-east-1.amazonaws.com/production/routine/updateExerciseList/${id}`,
                      {
                        method: "PUT",
                        body: JSON.stringify({ exerciseList: tempList }),
                        headers: { "Content-Type": "application/json" },
                      }
                    )
                      .then((res) => res.json())
                      .catch((err) => console.log(err));
                  } else {
                    tempList = data.warmupList;
                    tempList.push.apply(tempList, newWarmups);
                    await fetch(
                      `https://258dv5hvol.execute-api.us-east-1.amazonaws.com/production/routine/updateWarmupList/${id}`,
                      {
                        method: "PUT",
                        body: JSON.stringify({ warmupList: tempList }),
                        headers: { "Content-Type": "application/json" },
                      }
                    )
                      .then((res) => res.json())
                      .catch((err) => console.log(err));
                  }

                  setInstructions("");
                  setName("");
                  setReps(0);
                  setSets(0);
                  setRest("");
                  setVideoURL("");
                  setShowAddModal(false);
                  setShowAddModal(false);
                  router.reload();
                }}
              >
                Añadir
              </Button>
              <Button
                variant="secondary"
                onClick={() => setShowAddModal(false)}
              >
                Salir
              </Button>
            </Modal.Footer>
          </Modal>
        </>
      ) : null}

      {/* Add Modal */}
      {showAddActivityModal ? (
        <>
          <Modal
            show={showAddActivityModal}
            onHide={() => setShowAddActivityModal(false)}
            centered
          >
            <Modal.Header closeButton>
              <Modal.Title id="contained-modal-title-vcenter">
                Añade una Actividad
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <ReactSelect
                isMulti
                className={styles.select}
                options={activityOptions}
                onChange={(value) => {
                  setNewActivities(value);
                }}
              />
            </Modal.Body>
            <Modal.Footer>
              <Button
                variant="secondary"
                onClick={async () => {
                  let tempList = [];
                  tempList = data.activityList;
                  tempList.push.apply(tempList, newActivities);
                  await fetch(
                    `https://258dv5hvol.execute-api.us-east-1.amazonaws.com/production/routine/updateActivityList/${id}`,
                    {
                      method: "PUT",
                      body: JSON.stringify({ activityList: tempList }),
                      headers: { "Content-Type": "application/json" },
                    }
                  )
                    .then((res) => res.json())
                    .catch((err) => console.log(err));
                  router.reload();
                }}
              >
                Añadir
              </Button>
              <Button
                variant="secondary"
                onClick={() => setShowAddModal(false)}
              >
                Salir
              </Button>
            </Modal.Footer>
          </Modal>
        </>
      ) : null}
      {/* Create Modal */}
      {showCreateModal ? (
        <>
          <Modal
            show={showCreateModal}
            onHide={() => setShowCreateModal(false)}
            centered
          >
            <Modal.Header closeButton>
              <Modal.Title id="contained-modal-title-vcenter">
                Crea un {type === "exercise" ? "Ejercicio" : "Calentamiento"}
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <TextInput
                value={name}
                onChange={(event) => setName(event.target.value)}
                label="Nombre de ejercicio"
              />
              <TextArea
                value={instructions}
                rows={5}
                cols={41}
                onChange={(event) => setInstructions(event.target.value)}
                label="Instrucciones"
              />
              <TextInput
                value={reps}
                onChange={(event) => setReps(event.target.value)}
                label="Repeticiones"
                type="number"
              />
              <TextInput
                value={sets}
                onChange={(event) => setSets(event.target.value)}
                label="Sets"
                type="number"
              />
              <TextInput
                value={videoURL}
                onChange={(event) => setVideoURL(event.target.value)}
                label="Video de explicación"
                type="text"
              />
              <Select
                label="Descanso"
                value={rest}
                onChange={(event) => setRest(event.target.value)}
                options={[
                  "5 segundos",
                  "10 segundos",
                  "15 segundos",
                  "30 segundos",
                  "1 minuto",
                  "1:30 minuto",
                ]}
              />
            </Modal.Body>
            <Modal.Footer>
              <Button onClick={() => submitForm()}>Crear</Button>
              <Button type="secondary" onClick={() => setShowModal(false)}>
                Salir
              </Button>
            </Modal.Footer>
          </Modal>
        </>
      ) : null}

      {/* Create Modal */}
      {showCreateActivityModal ? (
        <>
          <Modal
            show={showCreateActivityModal}
            onHide={() => setShowCreateActivityModal(false)}
            centered
          >
            <Modal.Header closeButton>
              <Modal.Title id="contained-modal-title-vcenter">
                Crea una actividad
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <TextInput
                value={name}
                onChange={(event) => setName(event.target.value)}
                label="Nombre de actividad"
              />
              <TextArea
                value={instructions}
                onChange={(event) => setInstructions(event.target.value)}
                label="Instrucciones"
              />
              <TextInput
                value={reps}
                onChange={(event) => setReps(event.target.value)}
                label="Repeticiones"
                type="number"
              />
              <TextInput
                value={sets}
                onChange={(event) => setSets(event.target.value)}
                label="Sets"
                type="number"
              />
              <TextInput
                value={videoURL}
                onChange={(event) => setVideoURL(event.target.value)}
                label="Video de explicación"
                type="text"
              />
              <Select
                label="Descanso"
                value={rest}
                onChange={(event) => setRest(event.target.value)}
                options={[
                  "5 segundos",
                  "10 segundos",
                  "15 segundos",
                  "30 segundos",
                  "1 minuto",
                  "1:30 minuto",
                ]}
              />
              <TextInput
                value={tag}
                onChange={(event) => setTag(event.target.value.toUpperCase())}
                label="Tag"
                type="text"
              />
            </Modal.Body>
            <Modal.Footer>
              <Button onClick={() => submitActivity()}>Crear</Button>
              <Button type="secondary" onClick={() => setShowModal(false)}>
                Salir
              </Button>
            </Modal.Footer>
          </Modal>
        </>
      ) : null}

      {/* Edit Name Modal */}
      {editNameModal ? (
        <>
          <Modal
            show={editNameModal}
            onHide={() => setEditNameModal(false)}
            centered
          >
            <Modal.Header closeButton>
              <Modal.Title id="contained-modal-title-vcenter">
                Cambia el nombre de la rutina
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <TextInput
                value={name}
                onChange={(event) => setName(event.target.value)}
                label="Nombre de ejercicio"
              />
            </Modal.Body>
            <Modal.Footer>
              <Button onClick={() => updateName()}>Actualizar</Button>
              <Button type="secondary" onClick={() => setShowModal(false)}>
                Salir
              </Button>
            </Modal.Footer>
          </Modal>
        </>
      ) : null}
      <Container>
        <Box type="down">
          <div className={styles.taskbar}>
            <h2>Rutina: {routineName}</h2>
            <div className={styles.btnbox}>
              <Button onClick={() => router.back()}>Volver</Button>
              <Button onClick={() => setEditNameModal(true)}>Editar</Button>
              <Button type="danger" onClick={() => setShowModal(true)}>
                Borrar
              </Button>
            </div>
          </div>
          <div className={styles.description}>
            <div className={styles.header}>
              <h2>Día de Entrenamiento: {day}</h2>
              <Button onClick={() => setShowDayModal(true)}>Editar</Button>
            </div>

            <hr />
          </div>
          <div className={styles.description}>
            <div className={styles.header}>
              <h2>Instrucciones</h2>
              <Button onClick={() => setShowDescriptionModal(true)}>
                Editar
              </Button>
            </div>

            <hr />
            <p>{data.instructions}</p>
          </div>
          <div className={styles.routines}>
            <div className={styles.header}>
              <h2>Calentamientos</h2>
              <div className={styles.btnbox}>
                <Button
                  onClick={() => {
                    setType("warmup");
                    setShowAddModal(true);
                  }}
                >
                  Añadir
                </Button>
                <Button
                  onClick={() => {
                    setType("warmup");
                    setShowCreateModal(true);
                  }}
                >
                  Crear
                </Button>
              </div>
            </div>
            <hr />
            <div className={styles.grid}>
              {data.warmupList.map((warmup, key) => {
                let parsed = JSON.parse(warmup);
                return (
                  <div key={key} className={styles.label}>
                    <div
                      onClick={() =>
                        router.push(`/admin/exercises/${parsed.value}`)
                      }
                    >
                      {parsed.label}
                    </div>
                    <div
                      className={styles.remove}
                      onClick={() => {
                        setShowDeleteExercise(true);
                        setSelectedItem(parsed);
                        setType("calentamiento");
                      }}
                    >
                      X
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
          <div className={styles.routines}>
            <div className={styles.header}>
              <h2>Ejercicios</h2>
              <div className={styles.btnbox}>
                <Button
                  onClick={() => {
                    setType("exercise");
                    setShowAddModal(true);
                  }}
                >
                  Añadir
                </Button>
                <Button
                  onClick={() => {
                    setType("exercise");
                    setShowCreateModal(true);
                  }}
                >
                  Crear
                </Button>
              </div>
            </div>
            <hr />
            <div className={styles.grid}>
              {data.exerciseList.map((exercise, key) => {
                let parsed = JSON.parse(exercise);
                return (
                  <div key={key} className={styles.label}>
                    <div
                      onClick={() =>
                        router.push(`/admin/exercises/ver/${parsed.value}`)
                      }
                    >
                      {parsed.label}
                    </div>
                    <div
                      className={styles.remove}
                      onClick={() => {
                        setShowDeleteExercise(true);
                        setType("ejercicio");
                      }}
                    >
                      X
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
          <div className={styles.routines}>
            <div className={styles.header}>
              <h2>Actividades</h2>
              <div className={styles.btnbox}>
                <Button
                  onClick={() => {
                    setType("activity");
                    setShowAddActivityModal(true);
                  }}
                >
                  Añadir
                </Button>
                <Button
                  onClick={() => {
                    setType("activity");
                    setShowCreateActivityModal(true);
                  }}
                >
                  Crear
                </Button>
              </div>
            </div>
            <hr />
            <div className={styles.grid}>
              {data.activityList.map((exercise, key) => {
                let parsed = JSON.parse(exercise);
                return (
                  <div key={key} className={styles.label}>
                    <div
                      onClick={() =>
                        router.push(`/admin/activity/ver/${parsed.value}`)
                      }
                    >
                      {parsed.label}
                    </div>
                    <div
                      className={styles.remove}
                      onClick={() => {
                        setShowDeleteExercise(true);
                        setType("activity");
                      }}
                    >
                      X
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
        </Box>
      </Container>
      <Footer />
    </>
  );
}
