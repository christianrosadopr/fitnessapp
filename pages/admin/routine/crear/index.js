import Head from "next/head";
import styles from "../../../../styles/admin/program/Create.module.scss";
import {
  Box,
  Container,
  Button,
  TextInput,
  Footer,
  Navbar,
  TextArea,
  Select,
} from "../../../../components";
import { useRouter } from "next/router";
import { useState } from "react";
import ReactSelect from "react-select";


export async function getServerSideProps(context) {
  const res = await fetch(
    `${process.env.API}/exercises`
  );
  const data = await res.json();

  if (!data) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      data,
    }, // will be passed to the page component as props
  };
}

export default function CreateRoutine({ data }) {
  const router = useRouter();
  const [day, setDay] = useState(0);
  const [instructions, setInstructions] = useState("");
  const [routineName, setRoutineName] = useState("");
  const [exerciseList, setExerciseList] = useState([]);
  const [warmupList, setWarmupList] = useState([]);


  async function submitForm() {
    await fetch(`https://258dv5hvol.execute-api.us-east-1.amazonaws.com/production/routine`, {
      method: "POST",

      body: JSON.stringify({
        day: day,
        instructions: instructions,
        exerciseList: exerciseList,
        warmupList: warmupList,
        routineName: routineName,
      }),
      headers: { "Content-Type": "application/json" },
    })
      .then((res) => {
        res.json();
      })
      .then(() => router.back())
      .catch((err) => console.log(err))
  }

  let options = [];
  data.forEach((exercise) => {
    options.push({ value: exercise.exerciseID, label: exercise.exerciseName });
  });

  return (
    <>
      <Head>
        <title>Vendrell Method ADMIN: Crear Rutina</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Navbar />
      <Container>
        <Box type="down">
          <div className={styles.taskbar}>
            <h2>Crear una Rutina</h2>
          </div>
          <div className={styles.form}>
            <TextInput
              value={routineName}
              onChange={(event) => setRoutineName(event.target.value)}
              label="Nombre de rutina"
            />
            <TextInput
              value={day}
              onChange={(event) => setDay(event.target.value)}
              label="Día"
              type="number"
            />
            <TextArea
              value={instructions}
              onChange={(event) => setInstructions(event.target.value)}
              label="Instrucciones"
            />
            <label>Calentamiento</label>
            <ReactSelect
              isMulti
              className={styles.select}
              options={options}
              onChange={(value) => setWarmupList(value)}
            />
            <br />
            <label>Ejercicios</label>
            <ReactSelect
              className={styles.select}
              isMulti
              options={options}
              onChange={(value) => setExerciseList(value)}
            />
            <br />
            <Button onClick={() => submitForm()}>Crear</Button>
          </div>
        </Box>
      </Container>
      <Footer />
    </>
  );
}
