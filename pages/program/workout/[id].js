import Head from "next/head";
import Image from "next/image";
import styles from "../../../styles/Workout.module.scss";
import {
  Box,
  Container,
  Button,
  ExerciseCard,
  Footer,
  Navbar
} from "../../../components";
import { useRouter } from "next/router";
import ActivityCard from "../../../components/ActivityCard";

export async function getServerSideProps(context) {
  const { id } = context.params;
  const res = await fetch(`${process.env.API}/routine/${id}`);
  const data = await res.json();

  if (!data) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      data,
    }, // will be passed to the page component as props
  };
}

export default function Workout({ data }) {
  const router = useRouter();
  const { date } = router.query;

  const { exerciseList, warmupList, activityList } = data;

  return (
    <>
      <Head>
        <title>Area Sports: Fitness Program</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Container>
        <Box type="down">
          <div className={styles.header}>
            <h2>VENDRELL METHOD</h2>
            <h3>{date}</h3>
            <div className={styles.minibar}>
              <h4>EJERICIOS</h4>
              <Button onClick={() => router.back()}>Volver</Button>
            </div>
          </div>
          <div className={styles.grid}>
            <h4>CALENTAMIENTO</h4>
            {warmupList.map((exercise, key) => (
              <ExerciseCard exerciseID={JSON.parse(exercise).value} key={key}/>
            ))}
          </div>
          <div className={styles.grid}>
            <h4>EJERICIOS</h4>
            {exerciseList.map((exercise, key) => (
              <ExerciseCard exerciseID={JSON.parse(exercise).value} key={key}/>
            ))}
          </div>
          <div className={styles.grid}>
            <h4>ACTIVIDADES</h4>
            {activityList.map((activity, key) => (
              <ActivityCard activityID={JSON.parse(activity).value} key={key}/>
            ))}
          </div>
        </Box>
      </Container>
      <Footer />
    </>
  );
}
