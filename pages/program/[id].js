import Head from "next/head";
import Image from "next/image";
import styles from "../../styles/Program.module.scss";
import { Box, Container, RoutineCard, Navbar, Footer, Button } from "../../components";
import { useRouter } from "next/router";
import { useState, useEffect } from "react";

export async function getServerSideProps(context) {
  const { id } = context.params;
  const res = await fetch(`${process.env.API}/programs/${id}`);
  const data = await res.json();

  const { routineList } = data;
  let routineListData = [];
  const result = await routineList.forEach(async (routine) => {
    let parsed = JSON.parse(routine);
    routineListData.push(parsed.value);
  });

  if (!data) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      data,
      routineListData,
    }, // will be passed to the page component as props
  };
}

export default function Program({ data, routineListData }) {
  const router = useRouter();
  const [user, setUser] = useState();
  const [loading, setLoading] = useState(true);
   
  
  return (
    <>
      <Head>
        <title>Area Sports: Fitness Program</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Container>
        <Box type="down">
          <div className={styles.header}>
            <div className={styles.bar}>
              <h2>AREA SPORTS DEVELOPMENT</h2>
            </div>
            <h3>{data.programName}</h3>
          </div>
          <div className={styles.grid}>
            {routineListData.map((routine, key) => (
              <RoutineCard routineID={routine} key={key}/>
              // <p>{routine}</p>
            ))}
          </div>
        </Box>
      </Container>
      <Footer />
    </>
  );
}
