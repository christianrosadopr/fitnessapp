import Head from "next/head";
import Image from "next/image";
import styles from "../styles/Home.module.scss";
import { Box, Container, Button, TextInput, Footer } from "../components";
import { useState } from "react";
import { useRouter } from "next/router";
import { Modal } from "react-bootstrap";

export default function Login() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);
  const [showLoginModal, setShowLoginModal] = useState(false);

  const router = useRouter();

  async function login() {
    await fetch(
      `https://258dv5hvol.execute-api.us-east-1.amazonaws.com/production/users/auth/login`,
      {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          username: username,
          password: password,
        }),
      }
    ).then(res => res.json())
    .then(data => {
      sessionStorage.setItem("_USER_KEY_", data)
      if (data.username === "danielvendrell") {
        router.push('/admin')
      } else if (data.trainingProgram > 0) {
        router.push(`/program/${data.trainingProgram}`);
      }
      setShowLoginModal(false)

    })
    .catch(err => console.log(err))
  }
  return (
    <>
      <Head>
        <title>Vendrell Method: Fitness Program</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      {showLoginModal ? (
        <>
          <Modal
            show={showLoginModal}
            onHide={() => setShowLoginModal(false)}
            centered
          >
            <Modal.Body>
              <h2>Iniciar sesión</h2>
              <TextInput
                value={username}
                onChange={(event) => setUsername(event.target.value)}
                label="Username"
                type="text"
              />
              <TextInput
                value={password}
                onChange={(event) => setPassword(event.target.value)}
                label="Password"
                type="password"
              />
              <Button onClick={() => login()}>Entrar</Button>
            </Modal.Body>
            <Modal.Footer>
              <Button onClick={() => setShowLoginModal(false)}>Salir</Button>
            </Modal.Footer>
          </Modal>
        </>
      ) : null}
      <Container>
        <Box type="down">
          <div className={styles.box}>
            <img
              className={styles.image}
              src="/logo.png"
              alt="Area Sports Logo"
            />
            <h2>Bienvenidos al Performance App</h2>
            <p>Aquí somos los duros y por eso nos reconocen.</p>
            <br />
            <Button
              type={loading ? "disabled" : "primary"}
              onClick={() => setShowLoginModal(true)}
            >
              Iniciar sesión
            </Button>
            <Button type="ghost" onClick={() => router.push("/register")}>
              Crear cuenta
            </Button>
          </div>
        </Box>
      </Container>
      <Footer />
    </>
  );
}
