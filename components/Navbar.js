import Head from "next/head";
import Image from "next/image";
import styles from "../styles/components/Navbar.module.scss";
import { Button } from '.'
import { signOut } from "next-auth/react";
import { useRouter } from "next/router";

export default function Navbar() {
  const router = useRouter();
  return (<div className={styles.body}>
    <h2>VENDRELL METHOD</h2>
    <Button onClick={() => router.push('/')}>Sign out</Button>
  </div>);
}
