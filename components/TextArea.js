import React from "react";
import styles from "../styles/components/TextArea.module.scss";

export default function TextArea({ onChange, value, label, rows, cols }) {
  return (
    <div className={styles.body}>
      <label className={styles.label}>{label}</label>
      <br/>
      <textarea className={styles.textarea} rows={rows} cols={cols} value={value} onChange={onChange}/>
    </div>
  );
}

TextArea.defaultProps = {
  cols: 49,
  rows: 4
};