import styles from  '../styles/components/Container.module.scss';
import { useRouter } from 'next/router'

export default function Container ({ children }) {
  const router = useRouter();
  return (
    <div className={styles.container}>
      {children}
    </div>
  )
}