import React, { useEffect, useState } from "react";
import { Button } from ".";
import styles from "../styles/components/ExerciseCard.module.scss";
import axios from "axios";
import { useRouter } from "next/router";

export default function ActivityCard({ activityID, key }) {
  const [data, setData] = useState("");
  const [loading, setLoading] = useState(false);

  const router = useRouter();
  useEffect(() => {
    const fetchData = async () => {
      await axios
        .get(
          `https://258dv5hvol.execute-api.us-east-1.amazonaws.com/production/activity/${activityID}`
        )
        .then((res) => setData(res.data))
        .catch((err) => console.log(err))
        .finally(() => setLoading(false));
    };

    fetchData();
  }, [activityID]);
  const { activityName, videoURL, description, reps, sets, tag } = data;
  return (
    <div className={styles.card} key={key}>
      <h2>{activityName}</h2>
      <br />
      <h4>{tag}</h4>
      {videoURL === "TBD" ? (
        <p>No hay video</p>
      ) : (
        <iframe
          width="350"
          height="175"
          src={`https://www.youtube.com/embed/${videoURL}`}
          frameBorder="0"
          allowFullScreen
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
        ></iframe>
      )}
      <div className={styles.body}>
        <h3>Instrucciones</h3>
        <p>{description}</p>
        <table>
          <thead>
            <th>REPETICIONES</th>
            <th>SETS</th>
          </thead>
          <tbody>
            <tr>
              <td>{reps}</td>
              <td>{sets}</td>
            </tr>
          </tbody>
        </table>
        <Button onClick={() => router.push(`/program/workout/${exerciseID}`)}>
          COMPLETADO
        </Button>
      </div>
    </div>
  );
}
