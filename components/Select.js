import React from "react";
import styles from "../styles/components/Select.module.scss";

export default function Select({
  onChange,
  value,
  options,
  defaultValue,
  label
}) {
  return (
    <>
      {label ? <label>{label}</label> : null}
      <br />
      <select className={styles.select} onChange={onChange} value={value}>
        <option value="Escoge">Escoge</option>
        {options.map((option, key) => (
          <option key={key} value={option}>
            {option}
          </option>
        ))}
      </select>
      <br />
    </>
  );
}

Select.defaultProps = {
  type: "primary",
};
