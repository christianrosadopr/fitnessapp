import React, { useEffect, useState } from "react";
import { Button } from ".";
import styles from "../styles/components/RoutineCard.module.scss";
import axios from "axios";
import { useRouter } from "next/router";

export default function RoutineCard({ routineID, key }) {
  const [data, setData] = useState("");
  const [loading, setLoading] = useState(false);

  const router = useRouter();
  useEffect(() => {
    const fetchData = async () => {
      await axios
        .get(`https://258dv5hvol.execute-api.us-east-1.amazonaws.com/production/routine/${routineID}`)
        .then((res) => setData(res.data))
        .catch((err) => console.log(err))
        .finally(() => setLoading(false));
    };

    fetchData();
  }, [routineID]);

  const { day, exerciseList, warmupList, routineName } = data;
  return (
    <div className={styles.card} key={key}>
      <h2>Día {loading ? "Cargando..." : day}</h2>
      <div className={styles.body}>
        <h3>{loading ? "Cargando..." : routineName}</h3>
        {data ? (
          <ul>
            {exerciseList.map((exercise) => (
              <li key={JSON.parse(exercise).value}>
                {JSON.parse(exercise).label}
              </li>
            ))}
          </ul>
        ) : (
          <p>Cargando...</p>
        )}
        <Button
          onClick={() =>
            router.push(`/program/workout/${routineID}?date=${day}`)
          }
        >
          VER EJERICIOS
        </Button>
      </div>
    </div>
  );
}
