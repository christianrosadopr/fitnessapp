import React from "react";
import styles from "../styles/components/Button.module.scss";

export default function Button({ type, onClick, children, disabled }) {
  return (
    <button
      className={styles[`button_${type}`]}
      onClick={onClick}
      disabled={disabled}
    >
      {children}
    </button>
  );
}

Button.defaultProps = {
  type: "primary",
};
