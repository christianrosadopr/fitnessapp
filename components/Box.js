import styles from "../styles/components/Box.module.scss";
import { useRouter } from 'next/router'

export default function Box({ children, type }) {
  const router = useRouter();

  return (
    <>
      {type === 'center' ? <div className={styles.boxcenter}>{children}</div> : null}
      {type === 'right' ? <div className={styles.boxright}>{children}</div> : null}
      {type === 'left' ? <div className={styles.boxleft}>{children}</div> : null}
      {type === 'down' ? <div className={styles.boxdown}>{children}</div> : null}
    </>
    
  )
};

Box.defaultProps = {
  type: "center"
}