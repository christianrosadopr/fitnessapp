import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/components/Footer.module.scss'

export default function Footer() {
  return (
    <div className={styles.footer}>
      <p>Copyright © 2022. AREA Sports Development System™ All Right Reserved</p>
    </div>
  )
}