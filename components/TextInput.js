import React from "react";
import styles from "../styles/components/TextInput.module.scss";

export default function TextInput({ value, onChange, label, type}) {
  return (
    <>
      <label className={styles.label}>{label}</label>
      <br />
      <input
        className={styles.textinput}
        type={type}
        value={value}
        onChange={onChange}
      />
      <br/>
    </>
  );
}

TextInput.defaultProps = {
  type: "text",
};